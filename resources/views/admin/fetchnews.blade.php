@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>FEED POST</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Title</th>
                    <th scope="col">Content</th>
                    <th scope="col">Thumbnail</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td>{{ $d->title }}</td>
                        <td>{{ $d->content }}</td>
                        <td><a href="{{asset('uploads/post/'. $d->image)}}" target="_blank"><img src="{{asset('uploads/post/'. $d->image)}}" height="100" width="125"></a></td>
                        <td><a href="{{route('editnews', $d->id)}}" type="button" class="btn btn-warning">Edit</a></td>
                        <td><a href="{{route('deletenews', $d->id)}}" type="button" class="btn btn-danger">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
              {{ $data->links() }}
        </div>
    </main>
@endsection
@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>Edit {{$data->last_name}} {{$data->first_name}} ({{$data->stagename}}) Info</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
                <form action="{{route('posteditartist', $data->id)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="container">
                      <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label for="exampleInputEmail1">First Name</label>
                            <input class="form-control" name="first_name" id="exampleInputEmail1" type="text" placeholder="{{$data->first_name}}">
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Last Name</label>
                            <input type="text" name="last_name" class="form-control" id="exampleInputEmail1" placeholder="{{$data->last_name}}">
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Stage Name</label>
                            <input type="text" name="stagename" class="form-control" id="exampleInputEmail1" placeholder="{{$data->stagename}}">
                          </div>
                        </div>
                      </div>
                    </div>
                      <div class="container">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label for="exampleInputEmail1">Birthplace</label>
                              <input type="text" name="birthplace" class="form-control" id="exampleInputEmail1" placeholder="{{$data->birthplace}}">
                            </div>
                          </div>
                        <div class="col">
                            <div class="form-group">
                              <label for="example-date-input">Birthdate</label>
                                <input class="form-control" name="dob" type="date" value="{{$data->dob}}" id="example-date-input">
                            </div>
                        </div>
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description</label>
                        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3" placeholder="{{$data->description}}"></textarea>
                    </div>
                    <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="currentdp">Current Display Picture</label>
                                    <a href="{{asset('uploads/artist/'. $data->image)}}"><img src="{{asset('uploads/artist/'. $data->image)}}" height="100" width="125"></a> 
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleFormControlFile1">Image</label>
                                    <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
        </div>
    </main>
@endsection
@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>Edit Post {{$data->title}} #{{$data->id}}</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <form action="{{route('posteditnews', $data->id)}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <div class="form-group">
                    <label for="exampleInputEmail1">Post Title</label>
                    <input type="text" name="title" class="form-control" id="exampleInputEmail1" placeholder="{{$data->title}}">
                </div>
                <div class="form-group">
                    <label for="exampleFormControlTextarea1">Content</label>
                    <textarea class="form-control" name="content" id="exampleFormControlTextarea1" rows="3" placeholder="{{$data->content}}"></textarea>
                </div>
                <div class="container">
                    <div class="row">
                        <div class="col">
                            <div class="form-group">
                                <label for="currentdp">Current Thumbnail</label>
                                <a href="{{asset('uploads/post/'. $data->image)}}"><img src="{{asset('uploads/post/'. $data->image)}}" height="100" width="125"></a> 
                            </div>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="exampleFormControlFile1">Thumbnail</label>
                                <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-primary">Submit</button>
                </div>
                </form>
        </div>
    </main>
@endsection
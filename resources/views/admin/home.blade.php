@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
<main id="main">
    <div class="wrapper">
        <div class="container-fluid">
            <div class="row" id="layout-margin">
                <div class="col-md-6 col-xl-3">
                    <div class="card bg-dark text-center m-b-30">
                        <div class="mb-2 card-body text-white">
                            <h3 class="text-success">{{$stats[0]['posts']}}</h3>
                            <a href="{{route('getnews')}}"><h6 class="text-default">Posts</h6></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="card bg-dark text-center m-b-30">
                        <div class="mb-2 card-body text-white">
                            <h3 class="text-success">{{$stats[0]['users']}}</h3>
                            <a href="{{route('getusers')}}"><h6 class="text-default">Users</h6></a>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="card bg-dark text-center m-b-30">
                        <div class="mb-2 card-body text-white">
                            <h3 class="text-success">{{$stats[0]['artists']}}</h3>
                            <a href="{{route('getartist')}}"><h6 class="text-default">Artists</h6></a>    
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-3">
                    <div class="card bg-dark text-center m-b-30">
                        <div class="mb-2 card-body text-white">
                            <h3 class="text-success">{{$stats[0]['groups']}}</h3>
                            <a href="{{route('getgroup')}}"><h6 class="text-default">Groups</h6></a>
                        </div>
                    </div>
                </div>
            </div>
            <!-- end row -->
        
            <div class="row">
                <div class="col-xl-12">
                    <div class="card bg-dark m-b-30">
                        <div class="card-body">
                            <h4 class="mt-0 m-b-30 header-title">Latest User</h4>
        
                            <div class="table-responsive">
                                <table class="table m-t-20 mb-0 table-vertical">
                                        <thead>
                                            <tr>
                                                <th>ID</th>
                                                <th>Username</th>
                                                <th>First Name</th>
                                                <th>Last Name</th>
                                                <th>Email</th>
                                            </tr>
                                        </thead>
                                        
                                        <tbody>
                                    @foreach ($data as $d)
                                    <tr>
                                        <td>
                                            {{$d->id}}
                                        </td>
                                        <td>
                                            {{$d->username}}
                                        </td>
                                        <td>
                                            {{$d->first_name}}
                                        </td>
                                        <td>
                                            {{$d->last_name}}
                                        </td>
                                        <td>
                                            {{$d->email}}
                                        </td>
                                    </tr>
                                    @endforeach
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
        
            </div>
            <!-- end row -->
        </div>
    </div>
</main>
@endsection
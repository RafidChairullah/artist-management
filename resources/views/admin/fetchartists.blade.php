@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>List of Artist</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th class="text-center" scope="col">Fullname</th>
                    <th class="text-center" scope="col">Stagename</th>
                    <th class="text-center" scope="col">Birthdate</th>
                    <th class="text-center" scope="col">Description</th>
                    <th class="text-center" scope="col">Display Picture</th>
                    <th></th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    @php
                        $birth = $d->birthplace . ' ' . $d->dob;
                    @endphp
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td>{{ $d->fullname }}</td>
                        <td>{{ $d->stagename }}</td>
                        <td>{{ $birth }}</td>
                        <td>{{ $d->description }}</td>
                        <td><a href="{{asset('uploads/artist/'. $d->image)}}" target="_blank"><img src="{{asset('uploads/artist/'. $d->image)}}" height="100" width="125"></a></td>
                        
                        @if ($d->group_id == NULL)
                            <td><button class="btn btn-primary" data-toggle="modal" data-target="#myModal{{$d->id}}">Add to Group</button></td>
                        @else
                            <td><a href="{{route('removeartistgroup', $d->id)}}" class="btn btn-danger">Remove from Group</button></a>
                        @endif
                        
                        <!-- Modal -->
                        <div id="myModal{{$d->id}}" class="modal fade" role="dialog" data-backdrop="false">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Choose Group</h4>
                                    </div>
                                    <div class="modal-body">
                                      <div class="form-default">
                                          <form method="post" role="form"
                                              action="{{route('registerartistgroup', $d->id)}}">
                                              @csrf
                                              @method('PATCH')
                                                <div class="form-group">
                                                  <label for="gtype">Group</label>
                                                  <select class="form-control" id="gtype" name="group_id">
                                                    @foreach ($group as $g)
                                                      <option value="{{$g->id}}">{{$g->group_name}}</option>
                                                    @endforeach
                                                  </select>
                                                </div>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-default"
                                          data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-default">Confirm</button>
                                  </div>
                                  </form>
                                </div>
                            </div>
                        </div>

                        @if ($d->solo_id == NULL)
                            <td><button class="btn btn-primary" data-toggle="modal" data-target="#solomodal{{$d->id}}">Add Solo</button></td>
                        @else
                            <td><a href="{{route('removeartistsolo', $d->id)}}" class="btn btn-danger">Remove Solo</button></a>
                        @endif
                        
                        <!-- Modal -->
                        <div id="solomodal{{$d->id}}" class="modal fade" role="dialog" data-backdrop="false">
                            <div class="modal-dialog">

                                <!-- Modal content-->
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h4 class="modal-title">Choose Solo</h4>
                                    </div>
                                    <div class="modal-body">
                                      <div class="form-default">
                                          <form method="post" role="form"
                                              action="{{route('registerartistsolo', $d->id)}}">
                                              @csrf
                                              @method('PATCH')
                                                <div class="form-group">
                                                  <label for="gtype">Solo</label>
                                                  <select class="form-control" id="gtype" name="solo_id">
                                                    @foreach ($solo as $s)
                                                      <option value="{{$s->id}}">{{$s->group_name}}</option>
                                                    @endforeach
                                                  </select>
                                                </div>
                                      </div>
                                  </div>
                                  <div class="modal-footer">
                                      <button type="button" class="btn btn-default"
                                          data-dismiss="modal">Close</button>
                                      <button type="submit" class="btn btn-default">Confirm</button>
                                  </div>
                                  </form>
                                </div>
                            </div>
                        </div>

                        <td><a href="{{route('editartist', $d->id)}}" type="button" class="btn btn-warning">Edit</a></td>
                        <td><a href="{{route('deleteartist', $d->id)}}" type="button" class="btn btn-danger">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
              {{ $data->links() }}
        </div>
    </main>
@endsection
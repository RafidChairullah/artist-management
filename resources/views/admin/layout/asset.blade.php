<!DOCTYPE html>
<html lang="en">


<!-- Mirrored from ombre.wp4life.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 25 Aug 2020 02:46:52 GMT -->
<head>
    <title>SF Entertainment</title>
    <!-- META TAGS -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" />
    <meta name="description" content="">
    <meta name="keywords" content="">
    <!-- FAVICON -->
    <link href="{{asset('assets/images/favicon.ico')}}" rel="shortcut icon" type="image/x-icon" />
    <!-- CSS FILES -->
    <link rel="stylesheet" href="{{asset('css/app.css')}}">
    <link href="{{asset('assets/css/layouts.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/normalize.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/fakeloader.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/font-awesome.min.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/animations.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/tooltipster.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/style.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/nerveslider.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/owl.carousel.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/colors.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/media.css')}}" rel="stylesheet" type="text/css">
    <link href="{{asset('assets/css/map.css')}}" rel="stylesheet" type="text/css">

    <!--[if lt IE 9]>
       <script type="text/javascript" src="js/html5.js"></script>
    <![endif]-->
    <!-- NOSCRIPT -->
<noscript>
    <link href="{{asset('assets/css/nojs.css')}}" rel="stylesheet" type="text/css">
</noscript>

</head>

<body>
    <!-- FAKE LOADER -->
    <div id="fakeloader"></div>
    <!-- SEMI TRANSPARENT DARK BG COLOR -->
    <div id="bg-transparent"></div>
    <!-- MAIN CONTAINER -->
    <div id="main-container">
        <!-- HEADER -->
        <header id="header">
            <div id="header-top">
                <!-- LOGO -->
                <div id="header-logo">
                    <a href="{{route('home')}}">
                        <img src="{{asset('assets/images/logo.png')}}" alt="" />
                    </a>
                </div>
            
                <div id="header-icons">
                    <nav>
                        <ul class="nav">
                            <li>
                                <a href="{{route('signout')}}">
                                    {{Session::get('user')->username}}
                                </a>
                            </li>
                        </ul>
                    </nav>
                </div>    

            <div id="header-menu">
                <!-- MOBILE MENU ICON -->
                <a class="toggleMenu" href="#">MENU</a>
                <!-- MAIN MENU -->
                <nav>
                    <ul class="nav">
                        <li>
                            <a href="{{route('home')}}">HOME</a>
                        </li>
                        <li>
                            <a href="#">POSTS</a>
                            <ul>
                                <li>
                                    <a href="{{route('getnews')}}">List All Posts</a>
                                </li>
                                <li>
                                    <a href="{{route('addnews')}}">Create New Post</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="">ARTIST</a>
                            <ul>
                                <li>
                                    <a href="{{route('getartist')}}">View All Artists</a>
                                </li>
                                <li>
                                    <a href="{{route('addartist')}}">Add New Artist</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="">GROUP</a>
                            <ul>
                                <li>
                                    <a href="{{route('getgroup')}}">View All Groups</a>
                                </li>
                                <li>
                                    <a href="{{route('addgroup')}}">Add New Group</a>
                                </li>
                            </ul>
                        </li>
                        <li>
                            <a href="">FAQS</a>
                        <ul>
                            <li>
                                <a href="{{route('getfaqs')}}">View All FAQs</a>
                            </li>
                            <li>
                                <a href="{{route('addfaqs')}}">Add New FAQ</a>
                            </li>
                        </ul>
                        </li>
                    </li>
                        <li>
                            <a href="#">RESOURCES</a>
                            <ul>
                                <li>
                                    <a href="{{route('addimages')}}">Add Images</a>
                                </li>
                            </ul>
                        </li>
                    </ul>
                </nav>
            </div>
        </header>

        @yield('asset')
        
        <!-- FOOTER -->
        <footer id="footer">
            <!-- BACK TO TOP BUTTON -->
            <div id="back-to-top" class="tooltip-gototop" title="Back to Top"></div>
            <!-- FOOTER WIDGETS -->
            <div id="footer-widgets" class="grid">
                <!-- WIDGET 1 -->
                <div class="footer-widget unit one-third">
                    <div class="mapouter">
                        <div class="gmap_canvas">
                            <iframe width="401" height="310" id="gmap_canvas" src="https://maps.google.com/maps?q=B1%2C%207%2C%20Janghan-ro%2C%2020-gil%2C%20Dongdaemun-gu&t=&z=13&ie=UTF8&iwloc=&output=embed" frameborder="0" scrolling="no" marginheight="0" marginwidth="0">
                            </iframe>
                        </div>
                    </div>
                </div>
                <!-- WIDGET 2 -->
                <div class="footer-widget unit one-third">
                    <h5>Contact Us</h5>
                    <p>SF Entertainment</p>
                    <p><span class="fa fa-map-marker"></span>B1, 7, Janghan-ro, 20-gil, Dongdaemun-gu
                    </p>
                    <p><span class="fa fa-phone"></span>123-456-789
                    </p>
                    <p><span class="fa fa-paper-plane"></span>help@sfent.com
                    </p>
                </div>
                <!-- WIDGET 3 -->
                <div class="footer-widget unit one-third">
                    <h5>Social Media</h5>
                    <!-- TAGS -->
                    <div class="tags-container">
                        <a class="fa fa-facebook-f tooltip-pink" href="#">Facebook</a>
                        <a class="fa fa-twitter tooltip-pink" href="#">Twitter</a>
                        <a class="fa fa-instagram tooltip-pink" href="#">Instagram</a>
                    </div>
                </div>
            </div>
            <div class="footer-info">
                <!-- COPYRIGHT -->
                <div class="credits">SF Entertainment 2020</div>
                <!-- SOCIAL ICONS -->
                <ul class="social-icons footer-social">
                    <li>
                        <a href="#" class="fa fa-facebook-f tooltip-pink" title="Facebook">Facebook</a>
                    </li>
                    <li>
                        <a href="#" class="fa fa-twitter tooltip-pink" title="Twitter">Twitter</a>
                    </li>
                    <li>
                        <a href="#" class="fa fa-google-plus tooltip-pink" title="Google Plus">Google</a>
                    </li>
                    <li>
                        <a href="#" class="fa fa-instagram tooltip-pink" title="Instagram">Instagram</a>
                    </li>
                    <li>
                        <a href="#" class="fa fa-vimeo tooltip-pink" title="Vimeo">Vimeo</a>
                    </li>
                </ul>
            </div>
        </footer>
    </div>

    {{-- AJAX AND BOOTSTRAP --}}
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" integrity="sha384-vFJXuSJphROIrBnz7yo7oB41mKfc8JzQZiCq4NCceLEaO4IHwicKwpJf9c9IpFgh" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-beta.2/js/bootstrap.min.js" integrity="sha384-alpBpkh1PFOepccYVYDB4do5UnbKysX5WZXm3XxPqe5iKTfUKjNkCk9SaVuEZflJ" crossorigin="anonymous"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.js"></script>
    
    <!-- MODAL -->
    <script type="text/javascript" src="{{asset('assets/js/modal.js')}}"></script>
    <!-- CAROUSELS -->
    <script type="text/javascript" src="{{asset('assets/js/carousels.js')}}"></script>
    <!-- MAIN JS FILES -->
    <script type="text/javascript" src="{{asset('assets/js/jquery-1.11.3.min.js')}}"></script>
    <!-- FAKE LOADER -->
    <script type="text/javascript" src="{{asset('assets/js/fakeloader.js')}}"></script>
    <!-- NERVESLIDER -->
    <script type="text/javascript" src="{{asset('assets/js/jquery-ui-custom.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/nerveslider.min.js')}}"></script>
    <script type="text/javascript" src="{{asset('assets/js/homeslider.js')}}"></script>
    <!-- CAROUSELS -->
    <script type="text/javascript" src="{{asset('assets/js/owl.carousel.min.js')}}"></script>
    <!-- BACKSTRETCH (BG IMAGES) -->
    <script type="text/javascript" src="{{asset('assets/js/backstretch.min.js')}}"></script>
    <!-- TOOLTIPS -->
    <script type="text/javascript" src="{{asset('assets/js/jquery.tooltipster.min.js')}}"></script>
    <!-- CUSTOM JS -->
    <script type="text/javascript" src="{{asset('assets/js/custom.js')}}"></script>

    {{-- Sweet Alert --}}
    <script src="https://cdn.jsdelivr.net/npm/sweetalert2@7.12.15/dist/sweetalert2.all.min.js"></script>

    <?php if($message = Session::get('success')){
        echo '<script language="javascript">swal({
            title: "Success!",
            text: "'.$message.'",
            type: "success"
            });</script>';
        } ?>

            <?php if($message = Session::get('error')){
        echo '<script language="javascript">swal({
            title: "Error!",
            text: "'.$message.'",
            type: "error"
            });</script>';
        }
    ?>
</body>


<!-- Mirrored from ombre.wp4life.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Tue, 25 Aug 2020 02:48:03 GMT -->
</html>
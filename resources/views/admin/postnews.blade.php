@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>Blog</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
                <form action="{{route('uploadnews')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="exampleInputEmail1">Post Title</label>
                      <input type="text" name="title" class="form-control" id="exampleInputEmail1">
                    </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Content</label>
                        <textarea class="form-control" name="content" id="exampleFormControlTextarea1" rows="3"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlFile1">Thumbnail</label>
                        <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                      </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
        </div>
    </main>
@endsection
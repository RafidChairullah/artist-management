@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>Image Upload</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <form method="post" action="{{route('postimages')}}" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="container">
                    <div class="row">
                        <div class="col">
                            <label for="ct">Category</label>
                            <select class="form-control" id="ct" name="category">
                                  <option value="group_gallery">group_gallery</option>
                              </select>
                        </div>
                        <div class="col">
                            <div class="form-group">
                                <label for="ctid">Category ID</label>
                                <input type="number" value="1" min="1" name="fgn_id" class="form-control" id="ctid">
                            </div>
                        </div>
                        </div>
                    </div>
                    </div>
                    <div class="form-group">
                        <input class="form-control" type="file" name="filename[]" multiple>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-info" style="margin-top:12px"><i class="glyphicon glyphicon-check"></i> Submit</button>
                    </div>
                </div>
            </form>        
        </div>
    </main>
@endsection
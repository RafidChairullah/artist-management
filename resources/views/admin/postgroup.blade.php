@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>New Group</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
                <form action="{{route('postgroup')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <input type="hidden" value="true" name="status">
                    <div class="container">
                      <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label for="gname">Group Name</label>
                            <input type="text" name="group_name" class="form-control" id="gname">
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="fname">Fandom Name</label>
                            <input type="text" name="fandom_name" class="form-control" id="fname">
                          </div>
                        </div>
                        </div>
                      </div>
                    </div>
                      <div class="container">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                                <label for="gtype">Group Type</label>
                                <select class="form-control" id="gtype" name="grouptype">
                                  <option value="girlgroup">Girlgroup</option>
                                  <option value="boygroup" >Boygroup</option>
                                  <option value="solo">Soloist</option>
                                </select>
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                                <label for="ddate">Debut Date</label>
                                <input class="form-control" name="debut" type="date" value="1900-01-01" id="example-date-ddate">
                            </div>
                          </div>
                        </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description" rows="3"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="img">Image</label>
                        <input type="file" name="image" class="form-control-file" id="img">
                      </div>
                    <button type="submit" class="btn btn-primary">Post</button>
                  </form>
        </div>
    </main>
@endsection
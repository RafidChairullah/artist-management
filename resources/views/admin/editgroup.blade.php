@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>Edit {{$data->grouptype}} : {{$data->group_name}}</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
                <form action="{{route('posteditgroup', $data->id)}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="container">
                      <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label for="gname">Group Name</label>
                            <input type="text" name="group_name" class="form-control" id="gname" placeholder="{{$data->group_name}}">
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="fname">Fandom Name</label>
                            <input type="text" name="fandom_name" class="form-control" id="fname" placeholder="{{$data->fandom_name}}">
                          </div>
                        </div>
                        </div>
                      </div>
                    </div>
                      <div class="container">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                                <label for="gtype">Group Type</label>
                                <select class="form-control" id="gtype" name="grouptype">
                                  <option value="girlgroup">Girlgroup</option>
                                  <option value="boygroup" >Boygroup</option>
                                  <option value="solo">Soloist</option>
                                </select>
                              </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                                <label for="ddate">Debut Date</label>
                                <input class="form-control" name="debut" type="date" value="{{$data->debut}}" id="example-date-ddate">
                            </div>
                          </div>
                        </div>
                        <div class="form-group">
                            <label for="">Status</label>
                            <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" id="exampleRadios1" value="1" checked>
                                <label class="form-check-label" for="exampleRadios1">
                                  Active
                                </label>
                              </div>
                              <div class="form-check">
                                <input class="form-check-input" type="radio" name="status" id="exampleRadios2" value="0">
                                <label class="form-check-label" for="exampleRadios2">
                                  Inactive
                                </label>
                              </div>
                        </div>
                    <div class="form-group">
                        <label for="description">Description</label>
                        <textarea class="form-control" name="description" id="description" rows="3" placeholder="{{$data->description}}"></textarea>
                      </div>
                      <div class="container">
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="currentdp">Current Display Picture</label>
                                    <a href="{{asset('uploads/group/cover/'. $data->image)}}"><img src="{{asset('uploads/group/cover/'. $data->image)}}" height="100" width="125"></a> 
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="exampleFormControlFile1">Image</label>
                                    <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="submit" class="btn btn-primary">Post</button>
                  </form>
        </div>
    </main>
@endsection
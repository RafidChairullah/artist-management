@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>Details of {{$groupName->group_name}}</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th class="text-center" scope="col">ID</th>
                    <th class="text-center" scope="col">Date</th>
                    <th class="text-center" scope="col">Category</th>
                    <th class="text-center" scope="col">Description</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td>{{ $d->date }}</td>
                        <td>{{ $d->category }}</td>
                        <td>{{ $d->description }}</td>
                        <td><a href="" type="button" class="btn btn-warning">Edit</a></td>
                        <td><a href="" type="button" class="btn btn-danger">Delete</a></td>
                     </tr>                        
                    @endforeach
                </tbody>
              </table>

              <div class="d-flex flex-row-reverse">
                <div class="p-2">
                    <td><a href="{{route('addgroupdetail', $groupId)}}" type="button" class="btn btn-success">Add</a></td>
                </div>
              </div>
        </div>
    </main>
@endsection
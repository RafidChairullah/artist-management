@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>List of Group</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th class="text-center" scope="col">Groupname</th>
                    <th class="text-center" scope="col">Fandom</th>
                    <th class="text-center" scope="col">Debut</th>
                    <th class="text-center" scope="col">Type</th>
                    <th class="text-center" scope="col">Status</th>
                    <th class="text-center" scope="col">Members</th>
                    <th class="text-center" scope="col">Description</th>
                    <th class="text-center" scope="col">Display Picture</th>
                    <th class="text-center" scope="col">Detail</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    @php
                        if($d->status == TRUE){
                          $status = "Active";
                        }else {
                          $status = "Inactive";
                        }
                    @endphp
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td><a href="{{route('getgroupgallery', $d->group_name)}}">{{ $d->group_name }}</a></td>
                        <td>{{ $d->fandom_name }}</td>
                        <td>{{ $d->debut }}</td>
                        <td>{{ $d->grouptype }}</td>
                        <td>{{ $status }}</td>
                        <td>

                          <?php
                        foreach ($member as $m) {
                          if($m->group_id == $d->id){
                            echo $m->stagename . '<br><br>';
                          }
                        }
                        ?>

                        </td>
                        <td>{{ $d->description }}</td>
                        <td><img src="{{asset('uploads/group/cover/'. $d->image)}}" height="100" width="125"></td>
                        <td><a href="{{route('viewgroupdetail', $d->id)}}" type="button" class="btn btn-info">View</a></td>
                        <td><a href="{{route('editgroup', $d->id)}}" type="button" class="btn btn-warning">Edit</a></td>
                        <td><a href="{{route('deletegroup', $d->id)}}" type="button" class="btn btn-danger">Delete</a></td>
                      </tr>
                    @endforeach
                </tbody>
              </table>
              <div class="d-flex flex-row-reverse">
                <div class="p-2">
                    <td><a href="{{route('addgroup')}}" type="button" class="btn btn-success">Add</a></td>
                </div>
              </div>
              {{ $data->links() }}
        </div>
    </main>
@endsection
@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>New Artist</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
                <form action="{{route('postartist')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="container">
                      <div class="row">
                        <div class="col">
                          <div class="form-group">
                            <label for="exampleInputEmail1">First Name</label>
                            <input type="text" name="first_name" class="form-control" id="exampleInputEmail1">
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Last Name</label>
                            <input type="text" name="last_name" class="form-control" id="exampleInputEmail1">
                          </div>
                        </div>
                        <div class="col">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Stage Name</label>
                            <input type="text" name="stagename" class="form-control" id="exampleInputEmail1">
                          </div>
                        </div>
                      </div>
                    </div>
                      <div class="container">
                        <div class="row">
                          <div class="col">
                            <div class="form-group">
                              <label for="exampleInputEmail1">Birthplace</label>
                              <input type="text" name="birthplace" class="form-control" id="exampleInputEmail1">
                            </div>
                          </div>
                          <div class="col">
                            <div class="form-group">
                              <label for="example-date-input">Birthdate</label>
                                <input class="form-control" name="dob" type="date" value="1900-01-01" id="example-date-input">
                          </div>
                          </div>
                        </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Description</label>
                        <textarea class="form-control" name="description" id="exampleFormControlTextarea1" rows="3"></textarea>
                      </div>
                      <div class="form-group">
                        <label for="exampleFormControlFile1">Image</label>
                        <input type="file" name="image" class="form-control-file" id="exampleFormControlFile1">
                      </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
        </div>
    </main>
@endsection
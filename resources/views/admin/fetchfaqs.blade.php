@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>FAQs</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Category</th>
                    <th scope="col">Question</th>
                    <th scope="col">Answer</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td>{{ $d->category }}</td>
                        <td>{{ $d->question }}</td>
                        <td>{{ $d->answer }}</td>
                        <td><a href="{{route('editfaqs', $d->id)}}" type="button" class="btn btn-warning">Edit</a></td>
                        <td><a href="{{route('deletefaqs', $d->id)}}" type="button" class="btn btn-danger">Delete</a></td>
                    </tr>
                    @endforeach
                </tbody>
              </table>
              {{ $data->links() }}
        </div>
    </main>
@endsection
@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>List of Group</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th class="text-center" scope="col">Group ID</th>
                    <th class="text-center" scope="col">Groupname</th>
                    <th class="text-center" scope="col">Type</th>
                    <th class="text-center" scope="col">Gallery</th>
                    <th></th>
                    <th></th>
                  </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>{{ $group->id }}</td>
                        <td>{{ $group->group_name }}</td>
                        <td>{{ $group->grouptype }}</td>
                        <td>
                            <table class="table table-dark">
                                <thead>
                        @foreach ($image as $i)
                                <tr>
                                    <a href="" class="gallery photo" data-title="Lorem Ipsum Dolor">
                                        <img src="{{asset('uploads/category/'. $i->image)}}" height="200" width="485">
                                    </a>
                                </tr>
                                <br>
                                <tr><a href="/admin/getgroup/{{$group->group_name}}/gallery/{{$i->image}}/delete" type="button" class="btn btn-danger">Delete</a></tr>
                                <br><br>
                                </thead>
                        @endforeach
                            </table>
                        </td>
                        
                      </tr>
                </tbody>
              </table>
        </div>
    </main>
@endsection
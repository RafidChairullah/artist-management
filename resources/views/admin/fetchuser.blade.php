@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>Users</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <table class="table table-dark">
                <thead>
                  <tr>
                    <th scope="col">ID</th>
                    <th scope="col">Username</th>
                    <th scope="col">Email</th>
                    <th scope="col">First Name</th>
                    <th scope="col">Last Name</th>
                    {{-- <th></th> --}}
                  </tr>
                </thead>
                <tbody>
                    @foreach ($data as $d)
                    <tr>
                        <td>{{ $d->id }}</td>
                        <td>{{ $d->username }}</td>
                        <td>{{ $d->email }}</td>
                        <td>{{ $d->first_name }}</td>
                        <td>{{ $d->last_name }}</td>
                        {{-- <td><a href="{{route('deletenews', $d->id)}}" type="button" class="btn btn-danger">Ban</a></td> --}}
                    </tr>
                    @endforeach
                </tbody>
              </table>
              {{ $data->links() }}
        </div>
    </main>
@endsection
@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>FAQs</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
                <form action="{{route('uploadfaqs')}}" method="POST" enctype="multipart/form-data">
                    {{ csrf_field() }}
                    <div class="form-group">
                      <label for="exampleInputEmail1">Category</label>
                      <input type="text" name="category" class="form-control" id="exampleInputEmail1">
                    </div>
                    <div class="form-group">
                        <label for="exampleInputEmail1">Question</label>
                        <input type="text" name="question" class="form-control" id="exampleInputEmail1">
                      </div>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Answer</label>
                        <textarea class="form-control" name="answer" id="exampleFormControlTextarea1" rows="3"></textarea>
                      </div>
                    <button type="submit" class="btn btn-primary">Submit</button>
                  </form>
        </div>
    </main>
@endsection
@extends('admin.layout.asset')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <!-- PAGE TITLE -->
        <div id="page-title">
            <h1><span>Add Detail</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <form action="{{route('storegroupdetail', $groupId)}}" method="POST" enctype="multipart/form-data">
                {{ csrf_field() }}
                <input type="hidden" value="{{$groupId}}" name="group_id">
                <div class="container">
                    <div class="row">
                        <div class="form-group">
                            <label for="category">Category</label>
                            <select class="form-control" id="category" name="category">
                              <option value="Discography">Discography</option>
                              <option value="Awards" >Award</option>
                              <option value="Concerts">Concert</option>
                            </select>
                        </div>

                        <div class="col">
                            <div class="form-group">
                                <label for="description">Description</label>
                                <input type="text" name="description" class="form-control" id="description">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="form-group">
                    <label for="date">Date</label>
                    <input class="form-control" name="date" type="date" value="1990-01-01" id="date">
                </div>
                <button type="submit" class="btn btn-primary">Post</button>
                </form>
        </div>
    </main>
@endsection
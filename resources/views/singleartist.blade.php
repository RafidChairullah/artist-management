@extends('layout.prelogin')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        <div id="page-title">
            <h1><span>{{$data->group_name}}</span></h1>
        </div>
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <!-- GRID -->
            <div class="grid">
                <div class="unit golden-large">
                    <div>
                        <img src="{{asset('uploads/group/cover/'. $data->image)}}" height="450" width="650">
                    </div>
                    <div class="flex-video experience-video" data-pe-videoid="_gGYFFW3Ga0" data-thumb="images/photos/model-video.jpg"></div>
                    <!-- EXPERIENCES -->
                    <div class="experience-box">
                        <p>{{$data->description}}</p>
                    </div>
                    
                    <div class="container">
                        <h3 class="border">Discography</h3>
                        <div class="row">
                            <div class="col">
                                <p>2019.03</p>
                            </div>
                            <div class="col">
                                <p>Lorem ipsum</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p>2019.03</p>
                            </div>
                            <div class="col">
                                <p>Lorem ipsum</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p>2019.03</p>
                            </div>
                            <div class="col">
                                <p>Lorem ipsum</p>
                            </div>
                        </div>
                    </div>
                    
                    <div class="grid-border"></div>

                    <div class="container">
                        <h3 class="border">Awards</h3>
                        <div class="row">
                            <div class="col">
                                <p>2019.03</p>
                            </div>
                            <div class="col">
                                <p>Lorem ipsum</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p>2019.03</p>
                            </div>
                            <div class="col">
                                <p>Lorem ipsum</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p>2019.03</p>
                            </div>
                            <div class="col">
                                <p>Lorem ipsum</p>
                            </div>
                        </div>
                    </div>

                    <div class="grid-border"></div>

                    <div class="container">
                        <h3 class="border">Concert</h3>
                        <div class="row">
                            <div class="col">
                                <p>2019.03</p>
                            </div>
                            <div class="col">
                                <p>Lorem ipsum</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p>2019.03</p>
                            </div>
                            <div class="col">
                                <p>Lorem ipsum</p>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <p>2019.03</p>
                            </div>
                            <div class="col">
                                <p>Lorem ipsum</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="unit golden-small">

                    <div class="ombre-box">
                        <?php
                            if (sizeof($data2) > 0){
                                echo '
                                <div id="carousel-showcase" class="carousel slide" data-ride="carousel" data-interval="5000">
                                <ol class="carousel-indicators">
                                  <li data-target="#carousel-showcase" data-slide-to="0" class="active"></li>
                                ';

                                if($data2[1] != NULL){
                                    echo '<li data-target="#carousel-showcase" data-slide-to="1"></li>';
                                }
                                
                                if($data2[2] != NULL){
                                    echo '<li data-target="#carousel-showcase" data-slide-to="1"></li>';
                                }
                                
                                if($data2[3] != NULL){
                                    echo '<li data-target="#carousel-showcase" data-slide-to="1"></li>';
                                }
                
                                if($data2[4] != NULL){
                                    echo '<li data-target="#carousel-showcase" data-slide-to="1"></li>';
                                }
                                echo '
                                </ol>
                                    <div class="carousel-inner" role="listbox">
                                        <div class="carousel-item active">
                                            <img src="../uploads/category/'. $data2[0]->image.'" height="280" width="300" alt="First slide">
                                    </div>
                                ';

                                if($data2[1] != NULL){
                                    echo '<div class="carousel-item"><img src="../uploads/category/'. $data2[1]->image.'" height="280" width="300" alt="Second slide"></div>';
                                }
                                              
                                  
                                if($data2[2] != NULL){
                                    echo '<div class="carousel-item"><img src="../uploads/category/'. $data2[2]->image.'" height="280" width="300" alt="Third slide"></div>';
                                }
                                
                                if($data2[3] != NULL){
                                    echo '<div class="carousel-item"><img src="../uploads/category/'. $data2[3]->image.'" height="280" width="300" alt="Fourth slide"></div>';
                                }
                    
                                if($data2[4] != NULL){
                                    echo '<div class="carousel-item"><img src="../uploads/category/'. $data2[4]->image.'" height="280" width="300" alt="Fifth slide"></div>';
                                }
                                echo '
                                    </div>
                                        <a class="carousel-control-prev" href="#carousel-showcase" role="button" data-slide="prev">
                                            <span class="carousel-control-prev-icon"></span>
                                        </a>
                                        <a class="carousel-control-next" href="#carousel-showcase" role="button" data-slide="next">
                                                <span class="carousel-control-next-icon"></span>
                                        </a>
                                    </div>
                                ';
                        }
                        ?>
                        
                        <div class="grid-border"></div>

                        <!-- TABLE -->
                        <ul class="ombre-table">
                            <li>
                                <div class="ombre-table-left">Current State</div>
                                @if ($data->status == 0)
                                    <div class="ombre-table-right">Inactive</div>
                                @else
                                    <div class="ombre-table-right">Active</div>
                                @endif
                                
                            </li>
                            <li>
                                <div class="ombre-table-left">Debut</div>
                                <div class="ombre-table-right">{{$data->debut}}</div>
                            </li>
                            <li>
                                <div class="ombre-table-left">Fandom</div>
                                <div class="ombre-table-right">{{$data->fandom_name}}</div>
                            </li>
                            <h4 class="text-left"><span>Members</span></h4>
                            <div class="grid-border"></div>
                            @foreach ($data3 as $a)
                            <li>
                                <a href=""><div class="ombre-table-left">{{$a->stagename}}</div></a>
                            </li>
                            @endforeach
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </main>
@endsection
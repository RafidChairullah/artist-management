@extends('layout.prelogin')
@section('asset')
    <!-- MAIN -->
    <main id="main">
        {{-- <div class="grid-border"></div>
        <h1 class="text-center"><span>Featured Artists</span></h1>
       <div class="grid-border"></div> --}}
        <!-- FEATURED IMAGE -->
        <div id="carousel-example-generic" class="carousel slide" data-ride="carousel" data-interval="5000">
            <ol class="carousel-indicators">
              <li data-target="#carousel-example-generic" data-slide-to="0" class="active"></li>

              @if ($data[1] != NULL)
              <li data-target="#carousel-example-generic" data-slide-to="1"></li>
              @endif

              @if ($data[2] != NULL)
              <li data-target="#carousel-example-generic" data-slide-to="2"></li> 
              @endif
              
              @if ($data[3] != NULL)
              <li data-target="#carousel-example-generic" data-slide-to="3"></li>
              @endif

              @if ($data[4] != NULL)
              <li data-target="#carousel-example-generic" data-slide-to="4"></li> 
              @endif

            </ol>
            <div class="carousel-inner" role="listbox">
              <div class="carousel-item active">
                      <p class="carouselTitle"><a href="{{route('displaygroup', $data[0]->group_name)}}">{{$data[0]->group_name}}</a></p>
                <img src="{{asset('uploads/group/cover/'. $data[0]->image)}}" height="480" width="848" alt="First slide" onerror="this.onerror=null;this.src='http://www.rbbridge.com/wp-content/uploads/2019/11/BLACK-%ED%8B%B0%EC%A0%80-%EC%82%AC%EC%A7%84_1st-%EB%8B%A8%EC%B2%B42-1024x683.png';">
              </div>
              @if ($data[1] != NULL)
                <div class="carousel-item">
                    <p class="carouselTitle"><a href="{{route('displaygroup', $data[1]->group_name)}}">{{$data[1]->group_name}}</a></p>
                    <img src="{{asset('uploads/group/cover/'. $data[1]->image)}}" height="480" width="848" alt="Second slide">
                </div>           
              @endif
              
              @if ($data[2] != NULL)
                <div class="carousel-item">
                    <p class="carouselTitle"><a href="{{route('displaygroup', $data[2]->group_name)}}">{{$data[2]->group_name}}</a></p>
                    <img src="{{asset('uploads/group/cover/'. $data[2]->image)}}" height="480" width="848" alt="Third slide">
                </div>           
              @endif

              @if ($data[2] != NULL)
                <div class="carousel-item">
                    <p class="carouselTitle"><a href="{{route('displaygroup', $data[3]->group_name)}}">{{$data[3]->group_name}}</a></p>
                    <img src="{{asset('uploads/group/cover/'. $data[3]->image)}}" height="480" width="848" alt="Fourth slide">
                </div>           
              @endif
              
              @if ($data[3] != NULL)
                <div class="carousel-item">
                    <p class="carouselTitle"><a href="{{route('displaygroup', $data[4]->group_name)}}">{{$data[4]->group_name}}</a></p>
                    <img src="{{asset('uploads/group/cover/'. $data[4]->image)}}" height="480" width="848" alt="Fifth slide">
                </div>           
              @endif

            </div>
            <a class="carousel-control-prev" href="#carousel-example-generic" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#carousel-example-generic" role="button" data-slide="next">
                    <span class="carousel-control-next-icon"></span>
            </a>
          </div>

         <div class="grid-border"></div>
         <h1 class="text-center"><span>Values</span></h1>
        <div class="grid-border"></div>
          
        <!-- PAGE CONTAINER -->
        <div id="page-container">
            <!-- GRID -->
            <div class="grid">
                <div class="unit one-third">
                    <!-- ICON -->
                    <div class="icon-container">
                        <a href="#" class="fa fa-bomb"></a>
                    </div>
                    <!-- ICON TEXT-->
                    <div class="icon-text">
                        <h4>Viral</h4>
                        <p>Our talents are known whether in Asia or in the whole world.</p>
                    </div>
                </div>
                <div class="unit one-third">
                    <!-- ICON -->
                    <div class="icon-container">
                        <a href="#" class="fa fa-star"></a>
                    </div>
                    <!-- ICON TEXT -->
                    <div class="icon-text">
                        <h4>Quality</h4>
                        <p>We strive for quality rather than quantity in choosing our talents.</p>
                    </div>
                </div>
                <div class="unit one-third">
                    <!-- ICON -->
                    <div class="icon-container">
                        <a href="#" class="fa fa-bullhorn"></a>
                    </div>
                    <!-- ICON TEXT-->
                    <div class="icon-text">
                        <h4>Global</h4>
                        <p>We are everywhere across the whole world. We believe talent can come from anywhere.</p>
                    </div>
                </div>
                
            </div>
            <div class="grid-border"></div>
            <h1 class="text-left"><span>Latest News</span></h1>
            <div class="grid-border"></div>
            <!-- GRID -->
            <div class="grid">
                @foreach ($data2 as $d)
                @php
                    $formattedDate = date_format(date_create($d->created_at), 'd-m-Y');
                @endphp
                <div class="container">
                    <div class="row">
                        <div class="col-4">
                            <img src="{{asset('uploads/post/'. $d->image)}}" height="250" width="300">
                            
                        </div>
                        <div class="col-8">
                            <article class="grid-container">
                                <div class="grid-content">
                                    <div class="postdate">
                                        {{$formattedDate}}
                                    </div>
                                    <h4>
                                        <a href="">{{$d->title}}</a>
                                    </h4>
                                    <p>{{ \Illuminate\Support\Str::limit($d->content, 30, $end='...') }}</p>
                                </div>
                                <a class="arrow-button" href="">Read More</a>
                            </article>
                        </div>
                    </div>
                </div>
                
                @endforeach
            </div>
        </div>
    </main>
@endsection
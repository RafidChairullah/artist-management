// MODEL CAROUSEL
jQuery(window).load(function () {
    jQuery("#models").owlCarousel({
        items: 1,
        margin: 30,
        dots: false,
        smartSpeed: 800,
        navText: [' ', ' '],
        nav: true,
        loop: false,
        navRewind: false,
        lazyLoad: true,
        responsive: {
            481: {
                items: 1
            },
            641: {
                items: 2
            },
            1025: {
                items: 3
            }
        }
    });
});

// TESTIMONIAL CAROUSEL
jQuery(window).load(function () {
    "use strict";
    jQuery("#testimonials").show();
    var owl = jQuery("#testimonials").owlCarousel({
        items: 1,
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        dots: false,
        autoHeight: false,
        margin: 100,
        mouseDrag: false,
        touchDrag: false,
        smartSpeed: 800,
        nav: false,
        loop: true,
        animateIn: 'fadeIn',
        animateOut: 'fadeOut'
    });
    jQuery('#testimonials').find('.testimonial-nav-left .fa').click(function() {
        owl.trigger('prev.owl.carousel');
    });
    jQuery('#testimonials').find('.testimonial-nav-right .fa').click(function() {
        owl.trigger('next.owl.carousel');
    });
});

//OUR PARTNER CAROUSEL
jQuery(window).load(function () {
    "use strict";
    jQuery("#partners").owlCarousel({
        autoplay: true,
        autoplayTimeout: 4000,
        autoplayHoverPause: true,
        items: 1,
        margin: 20,
        dots: false,
        smartSpeed: 800,
        navText: ['', ''],
        nav: false,
        loop: true,
        lazyLoad: true,
        responsive: {
            341: {
                items: 2
            },
            641: {
                items: 3
            },
            1025: {
                items: 4
            }
        }
    });
});

// BACKSTRETCH
jQuery(window).load(function () {
    "use strict";
    jQuery('.grid.hero').backstretch("{{asset('assets/images/photos/testimonials.jpg')}}");
});


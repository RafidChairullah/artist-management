<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();

Route::get('/', 'HomeController@index')->name('home');
Route::get('/signup', 'auth\RegisterController@view')->name('signup');
Route::get('/signin', 'auth\LoginController@loginprocess')->name('loginprocess');
Route::get('/signout', 'HomeController@signout')->name('signout');

                        // Backend
Route::get('/admin', 'AdminController@view')->name('adminhome');
Route::get('/adminsignup', 'auth\RegisterController@viewadmin')->name('adminsignup');

//Blog Route
Route::get('/admin/addnews', 'BlogController@add')->name('addnews');
Route::post('/admin/addpost','BlogController@store')->name('uploadnews');
Route::get('/admin/getnews', 'BlogController@view')->name('getnews');
Route::get('/admin/editnews/{id}','BlogController@formedit')->name('editnews');
Route::post('/admin/editnews/{id}/change','BlogController@edit')->name('posteditnews');
Route::get('/admin/deletenews/{id}', 'BlogController@delete')->name('deletenews');

//Artist Route
Route::get('/admin/addartist', 'ArtistController@add')->name('addartist');
Route::post('/admin/postartist','ArtistController@store')->name('postartist');
Route::get('/admin/getartist', 'ArtistController@view')->name('getartist');
Route::patch('/admin/registerartisttogroup/{id}', 'ArtistController@registergroup')->name('registerartistgroup');
Route::get('/admin/removeartistfromgroup/{id}', 'ArtistController@removegroup')->name('removeartistgroup');
Route::patch('/admin/registerartisttosolo/{id}', 'ArtistController@registersolo')->name('registerartistsolo');
Route::get('/admin/removeartistfromsolo/{id}', 'ArtistController@removesolo')->name('removeartistsolo');
Route::get('/admin/editartist/{id}','ArtistController@formedit')->name('editartist');
Route::post('/admin/editartist/{id}/change','ArtistController@edit')->name('posteditartist');
Route::get('/admin/deleteartist/{id}', 'ArtistController@delete')->name('deleteartist');

//Group Route
Route::get('/admin/addgroup', 'GroupController@add')->name('addgroup');
Route::post('/admin/postgroup','GroupController@store')->name('postgroup');
Route::get('/admin/getgroup', 'GroupController@view')->name('getgroup');
Route::get('/admin/editgroup/{id}', 'GroupController@editpage')->name('editgroup');
Route::post('/admin/editgroup/{id}/change','GroupController@edit')->name('posteditgroup');
Route::get('/admin/deletegroup/{id}', 'GroupController@delete')->name('deletegroup');

//Group Detail Route
Route::get('/admin/group/viewdetail/{id}', 'GroupDetailController@view')->name('viewgroupdetail');
Route::get('/admin/group/adddetail/{id}', 'GroupDetailController@add')->name('addgroupdetail');
Route::post('/admin/group/adddetail/{id}/store', 'GroupDetailController@store')->name('storegroupdetail');

//FAQs Route
Route::get('/admin/addfaqs', 'FAQController@add')->name('addfaqs');
Route::post('/admin/addfaqs/store','FAQController@store')->name('uploadfaqs');
Route::get('/admin/getfaqs', 'FAQController@view')->name('getfaqs');
Route::get('/admin/editfaqs/{id}','FAQController@formedit')->name('editfaqs');
Route::post('/admin/editfaqs/{id}/change','FAQController@edit')->name('posteditfaqs');
Route::get('/admin/deletefaqs/{id}', 'FAQController@delete')->name('deletefaqs');

//Image Route
Route::get('/admin/addimages', 'ImageController@add')->name('addimages');
Route::post('/admin/postimages','ImageController@store')->name('postimages');

//Gallery Route
Route::get('/admin/getgroup/{name}/gallery', 'GroupController@viewgallery')->name('getgroupgallery');
Route::get('/admin/getgroup/{name}/gallery/{img}/delete', 'GroupController@removeimg')->name('deletegrouppicture');

//User Route
Route::get('/admin/getusers', 'UserController@view')->name('getusers');

                        // Frontend
Route::get('/group/{name}', 'HomeController@singlegroup')->name('displaygroup');
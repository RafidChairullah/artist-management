<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class GroupDetail extends Model
{
    protected $table = 'group_details';

    protected $fillable = ['category', 'group_id', 'date', 'description', 'deleted',];
}

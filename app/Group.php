<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Group extends Model
{
    protected $table = 'groups';

    protected $fillable = ['group_name', 'fandom_name', 'grouptype', 'status', 'debut', 'description', 'image'];

    public static function grouplist()
    {
        return Group::select('id', 'group_name')
        ->orderBy('debut', 'asc')
        ->get();
    }
}

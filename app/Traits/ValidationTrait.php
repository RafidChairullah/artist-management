<?php

namespace App\Traits;
use Illuminate\Support\Facades\Session;

trait ValidationTrait {

    public function checkadmin()
    {
        if(Session::has('user')){
            $user = Session::get('user');
            if($user->admin == FALSE){
                return FALSE;         
            }else{
                return TRUE;
            }
        }
    }
}
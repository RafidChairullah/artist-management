<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Artist extends Model
{
    protected $table = 'artists';

    protected $fillable = ['group_id', 'solo_id' , 'fullname', 'first_name', 'last_name', 'stage_name', 'birthplace', 'dob', 'description', 'image',];
}

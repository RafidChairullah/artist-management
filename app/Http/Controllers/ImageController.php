<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use App\Traits\ValidationTrait;

class ImageController extends Controller
{
    use ValidationTrait;
        /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function add() {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
          return view('admin.postimages');  
        }
    }

    public function store(Request $request) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            if($request->hasfile('filename')){
                foreach($request->file('filename') as $img){
                    $strid = substr(str_shuffle(str_repeat($x='0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ', ceil(24/strlen($x)) )),1,24);
                    $extension = $img->getClientOriginalExtension();
                    $filename = $strid.'.'. $extension;
                    $img->move('uploads/category/', $filename);
                    
                    $img = new Image();
                    $img->fgn_id = $request->input('fgn_id');
                    $img->category = $request->input('category');
                    $img->image = $filename;

                    $img->save();
                }
            }
            return redirect()->route('addimages')->with('success','Image has been uploaded');
        }
    }
}

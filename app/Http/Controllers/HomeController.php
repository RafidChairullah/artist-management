<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\DB;

Use Auth;
use App\Traits\ValidationTrait;

use app\Group;

class HomeController extends Controller
{
    use ValidationTrait;
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index(){
        if(Session::has('user')){
            $user = Session::get('user');
            if($user->admin == FALSE){
                return view('home');
            }else{
                $data = DB::table('users')
                ->select('id','first_name', 'last_name', 'email', 'username')
                ->where('admin', 0)
                ->paginate(5);

                $user = DB::table('users')
                ->where('admin', 0)
                ->count();

                $post = DB::table('blog_post')
                ->count();

                $artist = DB::table('artists')
                ->count();

                $group = DB::table('groups')
                ->count();

                $stats = [];

                array_push($stats, [
                        'users' => $user,
                        'posts' => $post,
                        'artists' => $artist,
                        'groups' => $group,
                ]);

                return view('admin.home', ['data' => $data, 'stats' => $stats]);
            }   
        }else{
            // Carousel data
            $data = DB::table('groups')
            ->select('group_name', 'image')
            ->where('status', 1)
            ->orderBy('debut', 'asc')
            ->paginate(5);

            //News Feed Data
            $data2 = DB::table('blog_post')
            ->select('id', 'title', 'content', 'image', 'created_at')
            ->orderBy('created_at', 'desc')
            ->paginate(3);

            return view('index', ['data' => $data, 'data2' => $data2]);
        }
    }

    public function signout(){
        Session::forget('user');
        Auth::logout();
        
        return redirect()->route('home')->with('success', 'You have signed out');
    }

    protected function singlegroup($name){
            $data = DB::table('groups')
            ->select('*')
            ->where('group_name', $name)
            ->first();

            // Carousel data
            $data2 = DB::table('images')
            ->select('image')
            ->where('category', 'group_gallery')
            ->where('fgn_id', $data->id)
            ->paginate(5);

            $data3 = DB::table('artists')
            ->select('id', 'stagename')
            ->where('group_id', $data->id)
            ->orWhere('solo_id', $data->id)
            ->orderBy('dob', 'asc')
            ->get();

            //Detail of Artist
            $discography = DB::table('group_details')
            ->select('date', 'description')
            ->where('group_id', $data->id)
            ->where('category', 'Discography')
            ->orderBy('date', 'desc')
            ->get();
            
            $awards = DB::table('group_details')
            ->select('date', 'description')
            ->where('group_id', $data->id)
            ->where('category', 'Awards')
            ->orderBy('date', 'desc')
            ->get();

            $concerts = DB::table('group_details')
            ->select('date', 'description')
            ->where('group_id', $data->id)
            ->where('category', 'Concerts')
            ->orderBy('date', 'desc')
            ->get();

            return view('singlegroup', [
                'data' => $data,
                'data2' => $data2, 
                'data3' => $data3,
                'discography' => $discography,
                'awards' => $awards,
                'concerts' => $concerts
                ]);
    }
}

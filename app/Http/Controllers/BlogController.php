<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

use App\Blog;
use App\Traits\ValidationTrait;

class BlogController extends Controller
{
    use ValidationTrait;
        /**
     * Create a new controller instance.
     *
     * @return void
     */

    public function view() {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $data = DB::table('blog_post')
            ->select('id', 'title', 'content', 'image')
            ->paginate(5);
            return view('admin.fetchnews', ['data'=>$data]);
        }   
    }

    public function formedit($id) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $data = DB::table('blog_post')
            ->select('*')
            ->where('id', $id)
            ->first();
            // dd($data);
            return view('admin.editnews', ['data'=>$data]);
        }   
    }

    protected function edit(Request $request, $id){
        // dd($request);

        $post = Blog::where('id', $id)->first();

        if($request->title == NULL){
            $post->title = $post->title;
        }else {
            $post->title = $request->title;
        }

        if($request->content == NULL){
            $post->content = $post->content;
        }else {
            $post->content = $request->content;
        }

        if($request->image == NULL){
            $post->image = $post->image;
        }else {
            // Remove Old Image
            $url = "uploads/post/" . $post->image;
            if(File::exists($url)) {
                File::delete($url);
            }

            //Post New Image
            if($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'. $extension;
                $file->move('uploads/post/', $filename);
                $post->image = $filename;

            }else{
                return $request;
                $post->image = '';
            }
        }
        $post->save();

        return redirect()->route('getnews')->with('success', 'Post has been successfully edited');

    }

    public function add() {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
          return view('admin.postnews');  
        }
    }

    public function store(Request $request) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $post = new Blog();

            $post->title = $request->input('title');
            $post->content = $request->input('content');
            $post->image = $request->input('image');

            if($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = time().'.'. $extension;
                $file->move('uploads/post/', $filename);
                $post->image = $filename;
            }else{
                return $request;
                $post->image = '';
            }

            $post->save();

            return redirect()->route('getnews')->with('success', 'Blog has been posted');
        }
        
    }

    protected function delete($id){
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{

            $post = Blog::where('id', $id)->first();

            $url = "uploads/post/" . $post->image;
            if(File::exists($url)) {
                File::delete($url);
            }
            $post->delete();
            return redirect()->route('getnews')->with('success', 'News successfully deleted from database');
        }
    }
}

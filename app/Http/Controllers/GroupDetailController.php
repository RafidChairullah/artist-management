<?php

namespace App\Http\Controllers;

use App\GroupDetail;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

use App\Traits\ValidationTrait;


class GroupDetailController extends Controller
{
    use ValidationTrait;

    protected function view($id){
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $data = DB::table('group_details')
            ->select('*')
            ->where('group_id', $id)
            ->get();

            $groupName = DB::table('groups')
            ->select('group_name')
            ->where('id', $id)
            ->first();

            return view('admin.fetchgroupdetail', ['data' => $data, 'groupId' => $id, 'groupName' => $groupName]);
        }
    }

    protected function add($id){
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            return view('admin.postgroupdetail', ['groupId' => $id]);
        }
    }

    protected function store(Request $request, $id){
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $this->validate($request, [
                'category' => 'required',
                'description' => 'required',
            ]);
            
            $detail = new GroupDetail;

            $detail->group_id = $request->input('group_id');
            $detail->category = $request->input('category');
            $detail->date = $request->input('date');
            $detail->description = $request->input('description');

            $detail->save();

            return redirect()->route('viewgroupdetail', $id)->with('success', 'Detail has been successfully added');
        }
    }
}

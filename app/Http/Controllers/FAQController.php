<?php

namespace App\Http\Controllers;

use App\FAQ;
use App\Traits\ValidationTrait;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FAQController extends Controller
{
    use ValidationTrait;

    public function view() {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $data = DB::table('faqs')
            ->select('id', 'category', 'question', 'answer')
            ->paginate(5);
            return view('admin.fetchfaqs', ['data'=>$data]);
        }   
    }

    public function add() {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
          return view('admin.postfaqs');  
        }
    }

    public function store(Request $request) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $this->validate($request, [
                'category' => 'required|alpha',
                'question' => 'required',
                'answer' => 'required',
            ]);

            $post = new FAQ();

            $post->category = $request->input('category');
            $post->question = $request->input('question');
            $post->answer = $request->input('answer');

            $post->save();

            return redirect()->route('getfaqs')->with('success', 'Blog has been posted');
        }
    }

    public function formedit($id) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $data = DB::table('faqs')
            ->select('*')
            ->where('id', $id)
            ->first();
            return view('admin.editfaqs', ['data'=>$data]);
        }   
    }

    protected function edit(Request $request, $id){
        // dd($request);

        $post = FAQ::where('id', $id)->first();

        if($request->category == NULL){
            $post->category = $post->category;
        }else {
            $post->category = $request->category;
        }

        if($request->question == NULL){
            $post->question = $post->question;
        }else {
            $post->question = $request->question;
        }

        if($request->answer == NULL){
            $post->answer = $post->answer;
        }else {
            $post->answer = $request->answer;
        }

        $post->save();

        return redirect()->route('getfaqs')->with('success', 'FAQ has been successfully edited');

    }

    protected function delete($id){
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{

            $post = FAQ::where('id', $id)->delete();
            return redirect()->route('getfaqs')->with('success', 'News successfully deleted from database');
        }
    }


}

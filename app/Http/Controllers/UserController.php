<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Traits\ValidationTrait;

class UserController extends Controller
{
    use ValidationTrait;

    protected function view(){
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $data = DB::table('users')
            ->select('id', 'first_name', 'last_name', 'username', 'email')
            ->where('admin', 0)
            ->paginate(10);

            return view('admin.fetchuser', ['data' => $data]);
        }
    }
}

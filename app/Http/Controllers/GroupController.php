<?php

namespace App\Http\Controllers;

use App\Group;
use App\Image;
use App\Artist;

use Illuminate\Http\Request;
use App\Traits\ValidationTrait;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;

class GroupController extends Controller
{
    use ValidationTrait;

    protected function view() {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $data = DB::table('groups')
            ->select('id', 'group_name', 'fandom_name', 'grouptype', 'status', 'debut', 'description', 'image')
            ->paginate(5);

            $member = DB::table('artists')
            ->select('stagename', 'group_id')
            ->whereNotNull('group_id')
            ->orderBy('dob', 'asc')
            ->get();
            return view('admin.fetchgroups', ['data'=>$data, 'member'=>$member]);
        }
    }

    protected function viewgallery($name) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $group = DB::table('groups')
            ->select('id', 'group_name', 'grouptype')
            ->where('group_name', $name)
            ->first();
            // dd($group);
            $image = DB::table('images')
            ->select('category', 'fgn_id', 'image')
            ->where('fgn_id', $group->id)
            ->where('category', 'group_gallery')
            ->get();

            return view('admin.fetchgroupgallery', ['group'=>$group, 'image'=>$image]);
        }
    }

    protected function store(Request $request) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $this->validate($data, [
                'group_name' => ['required', 'string', 'max:50', 'unique:groups'],
            ]);

            if($request->status == "true"){
               $status = TRUE; 
            }else{
                $status = FALSE;
            }

            $group = new Group();

            $group->group_name = $request->input('group_name');
            $group->fandom_name = $request->input('fandom_name');
            $group->grouptype = $request->input('grouptype');
            $group->status = $status;
            $group->debut = $request->input('debut');
            $group->description = $request->input('description');
            $group->image = $request->input('image');

            if($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = $group->group_name .'-'. 'main-picture' .'.'. $extension;
                $file->move('uploads/group/cover', $filename);
                $group->image = $filename;
            }else{
                return $request;
                $group->image = '';
            }

            $group->save();

            return redirect()->route('getgroup')->with('success', 'Group successfully added');
        }
    }

    protected function removeimg($name, $img) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $url = "uploads/category/" . $img;
            if(File::exists($url)) {
                File::delete($url);
                
                Image::where('image', $img)->delete();
                return redirect()->route('getgroupgallery', $name)->with('success', 'Image has been removed');
            }else{
                return redirect()->route('getgroupgallery', $name)->with('error', 'Oops, something went wrong');
            }
        }
    }

    protected function add() {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            return view('admin.postgroup');
        }
    }

    protected function editpage($id){
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $data = DB::table('groups')
            ->select('*')
            ->where('id', $id)
            ->first();

            return view('admin.editgroup', ['data' => $data]);
        }
    }

    protected function edit(Request $request, $id){
        // dd($request);

        $group = Group::where('id', $id)->first();

        if($request->group_name == NULL){
            $group->group_name = $group->group_name;
        }else {
            $group->group_name = $request->group_name;
        }

        if($request->fandom_name == NULL){
            $group->fandom_name = $group->fandom_name;
        }else {
            $group->fandom_name = $request->fandom_name;
        }

        if($request->grouptype == NULL){
            $group->grouptype = $group->grouptype;
        }else {
            $group->grouptype = $request->grouptype;
        }

        if($request->status == '0'){
            $group->status = FALSE;
        }else {
            $group->status = TRUE;
        }

        if($request->debut == NULL){
            $group->debut = $group->debut;
        }else {
            $group->debut = $request->debut;
        }

        if($request->description == NULL){
            $group->description = $group->description;
        }else {
            $group->description = $request->description;
        }

        if($request->image == NULL){
            $group->image = $group->image;
        }else {
            // Remove Old Image
            $url = "uploads/group/cover" . $group->image;
            if(File::exists($url)) {
                File::delete($url);
            }

            //Post New Image
            if($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = $group->group_name .'-'. 'main-picture' .'.'. $extension;
                $file->move('uploads/group/cover', $filename);
                $group->image = $filename;

            }else{
                return $request;
                $group->image = '';
            }
        }
        $group->save();

        return redirect()->route('getgroup')->with('success', 'Group has been successfully edited');

    }

    protected function delete($id){
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
                //Delete image resources DB
            $pics = DB::table('images')
            ->where('fgn_id', $id)
            ->get();
            foreach($pics as $img){
                $url = "uploads/category/" . $img->image;

                if(File::exists($url)) {
                    File::delete($url);

                    Image::where('image', $img->image)->delete();
                }
            }
                //Remove link to artist
            $nogroup = NULL;
            $artist = Artist::where('group_id', $id)->update([
                'group_id' => $nogroup,
            ]);


                //Delete Group
            $group = Group::where('id', $id)->first();

            $uri = "uploads/group/cover/" . $group->image;
            if(File::exists($uri)) {
                File::delete($uri);
            }
            $group->delete();

            return redirect()->route('getgroup')->with('success', 'Group has been deleted from database');
        }
    }
}
<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;


use App\Traits\ValidationTrait;
use App\Artist;

class ArtistController extends Controller
{
    use ValidationTrait;

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:50'],
            'last_name' => ['required', 'string', 'max:50'],
            'stagename' => ['required', 'string', 'max:25',],
        ]);
    }

    protected function view() {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $data = DB::table('artists')
            ->select('*')
            ->orderBy('created_at', 'desc')
            ->paginate(5);

            $group = DB::table('groups')
            ->select('id', 'group_name')
            ->where('grouptype', '!=' , 'solo')
            ->get();

            $solo = DB::table('groups')
            ->select('id', 'group_name')
            ->where('grouptype', 'solo')
            ->get();

            return view('admin.fetchartists', ['data'=>$data, 'group'=>$group, 'solo' => $solo]);
        }
    }

    protected function store(Request $request) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{

            $fullname =  $request->input('last_name') . ' ' . $request->input('first_name');
            
            $artist = new Artist();
            
            $artist->fullname = $fullname;
            $artist->first_name = $request->input('first_name');
            $artist->last_name = $request->input('last_name');
            $artist->stagename = $request->input('stagename');
            $artist->birthplace = $request->input('birthplace');
            $artist->dob = $request->input('dob');
            $artist->description = $request->input('description');
            $artist->image = $request->input('image');

            if($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = $artist->stagename .'-'. time().'.'. $extension;
                $file->move('uploads/artist/', $filename);
                $artist->image = $filename;
            }else{
                return $request;
                $artist->image = '';
            }

            $artist->save();

            return redirect()->route('getartist')->with('success', 'Artist successfully added');
        }
    }

    protected function add() {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            return view('admin.postartist');
        }
    }

    protected function formedit($id) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $data = DB::table('artists')
            ->select('*')
            ->where('id', $id)
            ->first();

            return view('admin.editartist', ['data' => $data]);
        }
    }

    protected function edit(Request $request, $id){
        // dd($request);

        $artist = Artist::where('id', $id)->first();

        if($request->first_name == NULL){
            $artist->first_name = $artist->first_name;
        }else {
            $artist->first_name = $request->first_name;
        }

        if($request->last_name == NULL){
            $artist->last_name = $artist->last_name;
        }else {
            $artist->last_name = $request->last_name;
        }

        if($request->stagename == NULL){
            $artist->stagename = $artist->stagename;
        }else {
            $artist->stagename = $request->stagename;
        }

        if($request->birthplace == NULL){
            $artist->birthplace = $artist->birthplace;
        }else {
            $artist->birthplace = $request->birthplace;
        }

        if($request->dob == NULL){
            $artist->dob = $artist->dob;
        }else {
            $artist->dob = $request->dob;
        }

        if($request->description == NULL){
            $artist->description = $artist->description;
        }else {
            $artist->description = $request->description;
        }

        if($request->image == NULL){
            $artist->image = $artist->image;
        }else {
            // Remove Old Image
            $url = "uploads/artist/" . $artist->image;
            if(File::exists($url)) {
                File::delete($url);
            }

            //Post New Image
            if($request->hasfile('image')) {
                $file = $request->file('image');
                $extension = $file->getClientOriginalExtension();
                $filename = $artist->stagename .'-'. time().'.'. $extension;
                $file->move('uploads/artist/', $filename);
                $artist->image = $filename;

            }else{
                return $request;
                $artist->image = '';
            }
        }
        $artist->save();
            return redirect()->route('getartist')->with('success', 'Artist has been successfully edited');

    }

    protected function registergroup(Request $request, $id) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $artist = Artist::where('id', $id)->update([
                'group_id' => $request->group_id,
            ]);
            return redirect()->route('getartist')->with('success', 'Artist successfully added to the group');
        }
    }

    protected function removegroup($id) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $artist = Artist::where('id', $id)->update([
                'group_id' => NULL,
            ]);
            return redirect()->route('getartist')->with('success', 'Artist successfully removed to the group');
        }
    }

    protected function registersolo(Request $request, $id) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $artist = Artist::where('id', $id)->update([
                'solo_id' => $request->solo_id,
            ]);
            return redirect()->route('getartist')->with('success', 'Artist successfully added to the group');
        }
    }

    protected function removesolo($id) {
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{
            $artist = Artist::where('id', $id)->update([
                'solo_id' => NULL,
            ]);
            return redirect()->route('getartist')->with('success', 'Artist successfully removed to the group');
        }
    }

    protected function delete($id){
        if($this->checkadmin() == FALSE){
            return redirect()->route('signout')->with('error', 'You are not authorized!');
        }else{

            $artist = Artist::where('id', $id)->first();

            $url = "uploads/artist/" . $artist->image;
            if(File::exists($url)) {
                File::delete($url);
            }
            $artist->delete();
            return redirect()->route('getartist')->with('success', 'Artist successfully deleted from database');
        }
    }
}
